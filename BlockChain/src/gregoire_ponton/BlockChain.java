package gregoire_ponton;

import java.security.Security;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.GsonBuilder;

public class BlockChain {
	
	public ArrayList<Block> blockchain = new ArrayList<Block>(); //Blockchain: liste des Blocks
	public static HashMap<String,TransactionOutput> UTXOs = new HashMap<String,TransactionOutput>(); //Liste des transactions non d�pens�es
	
	public int difficulty = 4; //Difficult� de minage. Rester entre 4-6 pour un minage rapide.
	//La difficult� Litecoin est aux alentours de 442 592
	
	//La difficult� d'une blockchain en production �volue en fonction de la valeur de la crypto-monnaie de la blockchain
	
	public static  float minimumTransaction = 0.1f; //La valeur minimum d'une transaction
	
	//Wallets utilis�s pour les transactions
	public Wallet walletA;
	public Wallet walletB;
	public Transaction genesisTransaction;
	
	//Fonction qui d�tecte si la blockchain est valide
	public  Boolean isChainValid() {
		
		//Blocs test�s
		Block currentBlock; 
		Block previousBlock;
		//Solution du proof of work
		String hashTarget = new String(new char[difficulty]).replace('\0', '0');
		//Liste temporaire des transactions non d�pens�es au moment de la cr�ation du bloc test�.
		HashMap<String,TransactionOutput> tempUTXOs = new HashMap<String,TransactionOutput>(); 
		//On ajoute les inputs et outputs de la transaction originelle.
		tempUTXOs.put(genesisTransaction.outputs.get(0).id, genesisTransaction.outputs.get(0));
		
		//On parcourt tout les blocs de la blockchain
		for(int i=1; i < blockchain.size(); i++) {
			
			currentBlock = blockchain.get(i);
			previousBlock = blockchain.get(i-1);
			
			//Ces tests servent � confirmer que personne n'a alt�r� les blocs.
			
			//Comparaison du hash enregistr� du bloc actuel
			if(!currentBlock.hash.equals(currentBlock.calculateHash()) ){
				System.out.println("Error: Hash calcul� diff�rent du Hash enregistr� au Bloc " + i);
				return false;
			}
			
			//Comparaison du hash pr�c�dent enregistr�e avec le hash du bloc pr�c�dent
			if(!previousBlock.hash.equals(currentBlock.previousHash) ) {
				System.out.println("Error: Hash du bloc " + (i-1) + " diff�rent du previous_hash du Bloc " + i);
				return false;
			}
			
			//Ce test sert � confirmer que le bloc a bien �t� min� avant d'�tre ajout� � la blockchain
			
			//Check proof of work
			if(!currentBlock.hash.substring( 0, difficulty).equals(hashTarget)) {
				System.out.println("Error: Le bloc " + i + " n'a pas �t� min�");
				return false;
			}
			
			//On parcoure les transactions du bloc actuel
			TransactionOutput tempOutput;
			for(int t=0; t <currentBlock.transactions.size(); t++) {
				Transaction currentTransaction = currentBlock.transactions.get(t);
				
				//Ces tests servent � confirmer que personne n'a alt�r� les transactions
				
				//V�rification de la signature de la transaction
				if(!currentTransaction.verifySignature()) {
					System.out.println("Error: La signature de la transaction " + t + " est invalide");
					return false; 
				}
				
				//V�rification que les inputs et outputs de la transaction concordent
				if(currentTransaction.getInputsValue() != currentTransaction.getOutputsValue()) {
					System.out.println("Error: Les inputs ne sont pas �gaux aux outputs de la transaction " + t);
					return false; 
				}
				
				//V�rification des inputs
				for(TransactionInput input: currentTransaction.inputs) {	
					tempOutput = tempUTXOs.get(input.transactionOutputId);
					
					if(tempOutput == null) {
						System.out.println("Error: La transaction " + t + " n'a pas d'input r�f�renc�");
						return false;
					}
					
					if(input.UTXO.value != tempOutput.value) {
						System.out.println("Error: L'input r�f�renc� de la transaction " + t + " est invalide");
						return false;
					}
					
					//Une fois test� on enl�ve l'input de la liste
					tempUTXOs.remove(input.transactionOutputId);
				}
				
				//V�rification des outputs
				for(TransactionOutput output: currentTransaction.outputs) {
					tempUTXOs.put(output.id, output);
				}
				
				if( currentTransaction.outputs.get(0).reciepient != currentTransaction.reciepient) {
					System.out.println("Error: Le destinataire de la transaction " + t + " n'est pas correct");
					return false;
				}
				if( currentTransaction.outputs.get(1).reciepient != currentTransaction.sender) {
					System.out.println("Error: L'output 'change' de la transaction " + t + " n'est pas l'envoyeur");
					return false;
				}
				
			}
			
		}
		System.out.println("La Blockchain est valide");
		return true;
	}
	
	//Fonction d'ajout d'un bloc � la blockchain
	public  void addBlock(Block newBlock) {
		//Proof of work
		newBlock.mineBlock(difficulty);
		
		blockchain.add(newBlock);
	}
	
	public Wallet setWalletA (int id) {
		
		walletA = new Wallet(id);
		return walletA;
		
	}
	
	public Wallet setWalletB (int id) {
		
		walletB = new Wallet(id);
		return walletB;
		
	}
	
	
	
}
