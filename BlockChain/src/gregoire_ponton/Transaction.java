package gregoire_ponton;
import java.security.*;
import java.util.ArrayList;

public class Transaction {
	
	public String transactionId; //Hash de la transaction
	public PublicKey sender; //Adresse de l'envoyeur/public key.
	public PublicKey reciepient; //Adresse du receveur/public key.
	public float value; //Valeur � envoy� au receveur
	public byte[] signature; //Signature, emp�che d'autres personnes de d�penser vos coins
	
	public ArrayList<TransactionInput> inputs = new ArrayList<TransactionInput>();
	public ArrayList<TransactionOutput> outputs = new ArrayList<TransactionOutput>();
	
	private  int sequence = 0; //Nombre de transactions g�n�r�es 
	
	//Constructeur 
	public Transaction(PublicKey from, PublicKey to, float value,  ArrayList<TransactionInput> inputs) {
		this.sender = from;
		this.reciepient = to;
		this.value = value;
		this.inputs = inputs;
	}
	
	//Transaction
	public boolean processTransaction() {
		
		if(verifySignature() == false) {
			System.out.println("Error: La signature de la transaction n'a pas pu �tre v�rifi�e");
			return false;
		}
				
		//R�cup�ration des inputs des transactions non d�pens�es
		for(TransactionInput i : inputs) {
			i.UTXO = BlockChain.UTXOs.get(i.transactionOutputId);
		}

		//V�rifie si la transaction est valide
		if(getInputsValue() < BlockChain.minimumTransaction) {
			System.out.println("Nombre entr� trop petit: " + getInputsValue());
			System.out.println("Entrez un nombre sup�rieur � " + BlockChain.minimumTransaction);
			return false;
		}
		
		//G�n�re les outputs de la transaction
		float leftOver = getInputsValue() - value;
		transactionId = calulateHash();
		//Envoie la valeur au receveur
		outputs.add(new TransactionOutput( this.reciepient, value,transactionId)); 
		//Renvoie le reste � l'envoyeur
		outputs.add(new TransactionOutput( this.sender, leftOver,transactionId)); 		
				
		//Ajoute les outputs � la liste des transactions non d�pens�es
		for(TransactionOutput o : outputs) {
			BlockChain.UTXOs.put(o.id , o);
		}
		
		//Enl�ve les outputs de la liste vu qu'ils ont �t� d�pens�es
		for(TransactionInput i : inputs) {
			//Si la transaction n'est pas trouv�e, on la saute
			if(i.UTXO == null) continue;
			BlockChain.UTXOs.remove(i.UTXO.id);
		}
		
		return true;
	}
	
	//Fonction r�cup�re les inputs
	public float getInputsValue() {
		float total = 0;
		for(TransactionInput i : inputs) {
			if(i.UTXO == null) continue;
			total += i.UTXO.value;
		}
		return total;
	}
	
	//Fonction g�n�ration de la signature
	public void generateSignature(PrivateKey privateKey) {
		String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(reciepient) + Float.toString(value)	;
		signature = StringUtil.applyECDSASig(privateKey,data);		
	}
	
	//Fonction v�rification de la signature
	public boolean verifySignature() {
		String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(reciepient) + Float.toString(value)	;
		return StringUtil.verifyECDSASig(sender, data, signature);
	}
	
	//Fonction r�cup�re les outputs
	public float getOutputsValue() {
		float total = 0;
		for(TransactionOutput o : outputs) {
			total += o.value;
		}
		return total;
	}
	
	//Fonction cr�� le hash de la transaction
	private String calulateHash() {
		//Sequence +1 pour �viter d'avoir des transactions avec le m�me hash
		sequence++; 
		return StringUtil.applySha256(
				StringUtil.getStringFromKey(sender) +
				StringUtil.getStringFromKey(reciepient) +
				Float.toString(value) + sequence
				);
	}
}
