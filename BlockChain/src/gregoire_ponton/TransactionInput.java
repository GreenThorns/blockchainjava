package gregoire_ponton;

public class TransactionInput {
	public String transactionOutputId; //Reference aux TransactionOutputs -> transactionId
	public TransactionOutput UTXO; //Contient l'output de la transaction non d�pens�e
	
	public TransactionInput(String transactionOutputId) {
		this.transactionOutputId = transactionOutputId;
	}
}
