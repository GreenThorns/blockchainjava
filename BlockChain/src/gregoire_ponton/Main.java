package gregoire_ponton;

import java.security.Security;

public class Main {
	
	static BlockChain blockchain = new BlockChain();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Setup BouncyCastle
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
				
		//Wallets
		Wallet wallet1 = blockchain.setWalletA(1);
		Wallet wallet2 = blockchain.setWalletB(2);
		
		//Block originel de la blockchain
		Block genesisBlock = genesisSetUp(wallet1);
				
		//Tests
		Block block1 = new Block(genesisBlock.hash);
		System.out.println("\nWallet1: " + wallet1.getBalance());
		System.out.println("\nTentative d'envoi de 40 de Wallet1 � Wallet 2");
		block1.addTransaction(wallet1.sendFunds(wallet2.publicKey, 40f));
		blockchain.addBlock(block1);
		System.out.println("\nWallet1: " + wallet1.getBalance());
		System.out.println("Wallet2: " + wallet2.getBalance());
				
		Block block2 = new Block(block1.hash);
		System.out.println("\nTentative d'envoi de 1000 de Wallet1 � Wallet 2");
		block2.addTransaction(wallet1.sendFunds(wallet2.publicKey, 1000f));
		blockchain.addBlock(block2);
		System.out.println("\nWallet1: " + wallet1.getBalance());
		System.out.println("Wallet2: " + wallet2.getBalance());
				
		Block block3 = new Block(block2.hash);
		System.out.println("\nTentative d'envoi de 20 de Wallet2 � Wallet 1");
		block3.addTransaction(wallet2.sendFunds( wallet1.publicKey, 20));
		System.out.println("\nWallet1: " + wallet1.getBalance());
		System.out.println("Wallet2: " + wallet2.getBalance());
				
		blockchain.isChainValid();
		
	}
	
	//Fonction Setup Block Originel + Transaction
	public static Block genesisSetUp(Wallet wallet) {
		
		Wallet coinbase = new Wallet(0);
		
		System.out.println("Cr�ation de la transaction originelle");
		//Cr�ation de la transaction qui envoie 100 � la wallet 
		blockchain.genesisTransaction = new Transaction(coinbase.publicKey, wallet.publicKey, 100f, null);
		//Signature manuelle de la transaction
		blockchain.genesisTransaction.generateSignature(coinbase.privateKey);
		//Set up manuel de l'id de la transaction
		blockchain.genesisTransaction.transactionId = "0";
		//Ajout manuel des outputs de la transaction
		blockchain.genesisTransaction.outputs.add(new TransactionOutput(blockchain.genesisTransaction.reciepient, blockchain.genesisTransaction.value, blockchain.genesisTransaction.transactionId));
		//Ajout de la transaction � la liste des transaction non d�pens�es
		blockchain.UTXOs.put(blockchain.genesisTransaction.outputs.get(0).id, blockchain.genesisTransaction.outputs.get(0));
		
		System.out.println("Cr�ation du bloc originel");
		Block genesis = new Block("0");
		//On ajoute la transaction au bloc
		genesis.addTransaction(blockchain.genesisTransaction);
		//On ajoute le bloc � la blockchain
		blockchain.addBlock(genesis);
		
		return genesis;
	}

}
