package gregoire_ponton;

import java.security.PublicKey;

public class TransactionOutput {
	public String id; //Transaction id
	public PublicKey reciepient; //Adresse qui recevra les coins
	public float value; //Nombre de coins � transf�rer
	public String parentTransactionId; //L'id de la transaction o� cet output a �t� cr��
	
	//Constructeur
	public TransactionOutput(PublicKey reciepient, float value, String parentTransactionId) {
		this.reciepient = reciepient;
		this.value = value;
		this.parentTransactionId = parentTransactionId;
		this.id = StringUtil.applySha256(StringUtil.getStringFromKey(reciepient)+Float.toString(value)+parentTransactionId);
	}
	
	//V�rifie si les coins m'appartiennent
	public boolean isMine(PublicKey publicKey) {
		return (publicKey == reciepient);
	}
	
}
