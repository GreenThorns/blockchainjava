package gregoire_ponton;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Wallet {
	
	public int id; //ID du porte-monnaie
	public PrivateKey privateKey; //Cl� priv�e - Sert pour signer les transactions
	public PublicKey publicKey; //Cl� publique - Sert d'adresse o� pour les transactions 
	
	//Liste des transactionsnon d�pens�es
	public HashMap<String,TransactionOutput> UTXOs = new HashMap<String,TransactionOutput>();
	
	//Constructeur
	public Wallet(int id) {
		this.id = id;
		generateKeyPair();
	}
	
	//Fonction g�n�ration d'une paire de cl�s encrypt�es
	public void generateKeyPair() {
		//technique ECDSA (Eliptic Curve Digital Signature Algorithm)
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("ECDSA","BC");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
			ECGenParameterSpec ecSpec = new ECGenParameterSpec("prime192v1");

			keyGen.initialize(ecSpec, random); //256 
	        KeyPair keyPair = keyGen.generateKeyPair();

	        privateKey = keyPair.getPrivate();
	        publicKey = keyPair.getPublic();
	        
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	//Fonction calcule les coins du porte-monnaie � partir des transactions non d�pens�es qui ont la signature de ce porte-monnaie
	public float getBalance() {
		float total = 0;	
        for (Map.Entry<String, TransactionOutput> item: BlockChain.UTXOs.entrySet()){
        	TransactionOutput UTXO = item.getValue();
            if(UTXO.isMine(publicKey)) {
            	UTXOs.put(UTXO.id,UTXO);
            	total += UTXO.value ; 
            }
        }  
		return total;
	}
	
	//Fonction g�n�re la transaction pour envoyer des fonds
	public Transaction sendFunds(PublicKey _recipient,float value ) {
		if(getBalance() < value) {
			System.out.println("Error: Wallet " + id + " n'a pas assez de fonds. Transaction annul�e.");
			return null;
		}
		ArrayList<TransactionInput> inputs = new ArrayList<TransactionInput>();
		
		float total = 0;
		for (Map.Entry<String, TransactionOutput> item: UTXOs.entrySet()){
			TransactionOutput UTXO = item.getValue();
			total += UTXO.value;
			inputs.add(new TransactionInput(UTXO.id));
			if(total > value) break;
		}
		
		Transaction newTransaction = new Transaction(publicKey, _recipient , value, inputs);
		newTransaction.generateSignature(privateKey);
		
		for(TransactionInput input: inputs){
			UTXOs.remove(input.transactionOutputId);
		}
		
		return newTransaction;
	}
	
}


