package gregoire_ponton;

import java.util.ArrayList;
import java.util.Date;

public class Block {
	
	public String hash; //Hash du bloc
	public String previousHash; //Hash du bloc pr�c�dent
	public String merkleRoot; //Version am�lior�e du hash qui contient toutes les transactions
	public ArrayList<Transaction> transactions = new ArrayList<Transaction>(); //Liste de transactions
	public long timeStamp; //Date de cr�ation du bloc
	public int nonce; //T�moin du minage du bloc
	
	//Constructeur  
	public Block(String previousHash ) {
		this.previousHash = previousHash;
		this.timeStamp = new Date().getTime();
		
		this.hash = calculateHash();
	}
	
	//Fonction calcule le hash du bloc
	public String calculateHash() {
		String calculatedhash = StringUtil.applySha256( 
				previousHash +
				Long.toString(timeStamp) +
				Integer.toString(nonce) + 
				merkleRoot
				);
		return calculatedhash;
	}
	
	//Fonction proof of Work
	public void mineBlock(int difficulty) {
		merkleRoot = StringUtil.getMerkleRoot(transactions);
		//Cr�� un String difficulty * "0" 
		String target = StringUtil.getDifficultyString(difficulty);
		while(!hash.substring( 0, difficulty).equals(target)) {
			nonce ++;
			hash = calculateHash();
		}
		System.out.println("Block Min�! : " + hash);
	}
	
	//Fonction Ajout de la transaction au Bloc
	public boolean addTransaction(Transaction transaction) {
		//Process la transaction et v�rifie si elle est valide
		if(transaction == null) return false;		
		if((!"0".equals(previousHash))) { //On saute le bloc originel
			if((transaction.processTransaction() != true)) {
				System.out.println("Erreur lors du process de la transaction. Annul�.");
				return false;
			}
		}

		transactions.add(transaction);
		System.out.println("Transaction aujout� au Block");
		return true;
	}
	
}
